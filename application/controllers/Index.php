<?
defined('BASEPATH') OR exit('No direct script access allowed');

final class Index extends CI_Controller
{
    /**
     * Index constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model("index_model");
        $ip = $this->index_model->getIp();
        $this->load->library("session");
        if (!isset($_COOKIE['registrado'])) {
            $_COOKIE['registrado'] = uniqid('', true);
            /* Creamos una cookie que dure un año */
            setcookie(
                'registrado',
                $_COOKIE['registrado'],
                time() + 3600 * 24
            );
            $this->index_model->insertIP($ip);
        }

    }

    public function index()
    {
        $this->load->view("parcials/header");
        $this->load->view("user/principal", array("user" => $this->index_model->selectUser("8", "users"),
            "capa" => $this->index_model->selectUser("8", "capacidades", "8"),
            "form" => $this->index_model->selectFormacion("8", "formacion"),
            "comp" => $this->index_model->selectUser("8", "complementaria", "8"),
            "exp" => $this->index_model->selectFormacion("8", "laboral"),
            "computing" => $this->index_model->selectFormacion("8", "informatica"),
            "lang" => $this->index_model->selectFormacion("8", "idiomas")
        ));
        $this->load->view("parcials/footer");

    }

    public function fechas($fecha_incio, $fechaFinal)
    {
        $datetime1 = new DateTime($fecha_incio);

        $datetime2 = new DateTime($fechaFinal);
        # obtenemos la diferencia entre las dos fechas
        $interval = $datetime2->diff($datetime1);
        # obtenemos la diferencia en meses
        $intervalMeses = $interval->format("%m");
        # obtenemos la diferencia en años y la multiplicamos por 12 para tener los meses
        $intervalAnos = $interval->format("%y");


    }

}

