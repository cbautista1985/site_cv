<?
defined('BASEPATH') OR exit('No direct script access allowed');

final class Index_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getIp(){
        if (!empty($_SERVER['HTTP_CLIENT_IP']))
            return $_SERVER['HTTP_CLIENT_IP'];

        if (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
            return $_SERVER['HTTP_X_FORWARDED_FOR'];

        return $_SERVER['REMOTE_ADDR'];

    }
    public function insertIP($ip){
            return $this->db->set(
                array(
                    "ip"=>$ip,
                )

            )->insert("visitas");
    }
    public function selectUser($id, $table,$idUser=null){
        if($idUser==null){
            return $this->db->get_where($table,array("id"=>$id))->row();
        }else{
            return $this->db->get_where($table,array("idUser"=>$id))->row();
        }



    }
    public function selectFormacion($id,$table){
         $query = $this->db->select("*")->from($table)->where("idUser",$id)->get();
         if($query->num_rows()>0){
             return $query->result();
         }
    }
    public function get($page, $segment,$table){

        return $this->db->get($table,$page,$segment)->result();

    }
    public function rows($table,$id){

        $query=  $this->db->select("*")->from($table)->where("idUser",$id);
        return $query->num_rows();
    }
    public function pagination($url,$table,$view){
        $this->load->library("pagination");
        $config["base_url"]=base_url($url);
        $config["total_rows"]=$this->rows($table,"8");
        $config["per_page"] = 8;
        $config['num_links'] = 20;
        $config["uri_segment"] = 3;
        $config['first_link']       = 'Primera';
        $config['last_link']        = 'Anterior';
        $config['next_link']        = 'Siguiente';
        $config['prev_link']        = 'Anterior';
        $config['full_tag_open']    = '<div class="pagging text-center " style="margin-top: 20px"><nav><ul class="pagination justify-content-center">';
        $config['full_tag_close']   = '</ul></nav></div>';
        $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close']    = '</span></li>';
        $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close']  = '</span>Next</li>';
        $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close']  = '</span></li>';



        $this->pagination->initialize($config);
        $this->load->view($view, array("computing"=>$this->get($config["per_page"]
            ,$this->uri->segment(3),$table)));
    }




}
//End of file applications/controller/Hello.php