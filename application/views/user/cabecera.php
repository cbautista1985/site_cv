<?php
/**
 * Created by PhpStorm.
 * User: logan
 * Date: 19/03/18
 * Time: 20:26
 */
?>
<div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12" >
        <div class="jumbotron principal" style="">
            <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-6 col-xl-6">
            <h1 class="display-3"><?=$user->nombre." ".$user->apellidos?></h1>
            <img src="<?=base_url()?>media/image/<?=$user->imagen?>" class="img-thumbnail" style="margin-top: 35px">
                <h6 style="color: white;!important; margin-top: 10px">Programador informático</h6>
            </div>

            <div class="col-sm-12 col-md-12 col-lgiv col-xl-6">
            <h6 style="color: white!important;"><?=$capa->nombre?></h6>
            </div>
            </div>
        </div>

    </div>
</div>
<div class="col-sm-12 col-md-12 col-lg-12 col-xl-12 text-center">
    <hr>
    <a class="link" href="https://www.linkedin.com/in/cristian-bautista-peral-59990065/" target="_blank"
       title="linkedin"><i class="fa fa-linkedin-square fa-2x"></i></a>
    <a class="facebook" target="_blank" href="https://www.facebook.com/toretobdn" title="Facebook"><i
                class="fa fa-facebook-square fa-2x"></i></a>
    <a class="bit" href="https://bitbucket.org/dashboard/overview" title="Bitbucket" target="_blank"><i
                class="fa fa-bitbucket fa-2x"></i></a>
    <hr>

</div>
