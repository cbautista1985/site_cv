<?php
/**
 * Created by PhpStorm.
 * User: logan
 * Date: 19/03/18
 * Time: 18:33
 */
?>


<main class="container">
    <?php $this->load->view("user/cabecera") ?>


    <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12 cajaLogin">
        <h1 class="text-center">Datos personales</h1>
        <hr>
        <div class="row" style="padding: 15px;">
            <div class="col-sm-12 col-md-12 col-lg-6 col-xl-6">
                <p><i class="fa fa-address-card"></i> Dirección: <?= $user->direccion ?></p>
                <p><i class="fa fa-address-card"></i> Provincia: <?= $user->provincia ?></p>
                <p><i class="fa fa-phone"></i> Telefóno:<?= $user->telefono ?></p>
                <p><i class="fa fa-mobile"></i> Móvil: <?= $user->mobil ?></p>


            </div>
            <div class="col-sm-12 col-md-12 col-lg-6 col-xl-6">
                <p><i class="fa fa-address-card-o"></i> NIF:<?= $user->nif ?></p>
                <p><i class="fa fa-google"></i> Email: <?= $user->email ?></p>

            </div>

        </div>


    </div>
    <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12 cajaLogin">
        <h1 class="text-center">Formación Académica</h1>
        <hr>
        <div class="row" style="padding: 15px;">
            <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <?
                foreach ($form as $value) {
                    ?>
                    <div class="row">
                        <div class="col-1">
                            <i class="fa fa-university fa-2x"></i>
                        </div>
                        <div class="col-11">
                            <p><?= $value->nombre ?></p>
                            <p><?
                                $datetime1=new DateTime($value->fecha_incio);

                                $datetime2=new DateTime($value->fecha_final);
                                # obtenemos la diferencia entre las dos fechas
                                $interval=$datetime2->diff($datetime1);
                                # obtenemos la diferencia en meses
                                $intervalMeses=$interval->format("%m");
                                # obtenemos la diferencia en años y la multiplicamos por 12 para tener los meses
                                $intervalAnos = $interval->format("%y");

                                if($intervalAnos!="0"){?>
                                <span>(<?=$intervalAnos." años y ".$intervalMeses." meses"?>)</span></p>
                                <?}else{?>
                                <span>(<?=$intervalMeses." meses"?>)</span></p>
                                <?}
                                ?>
                        </div>
                    </div>
                    <hr>


                    <?
                }
                ?>

            </div>


        </div>


    </div>
    <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12 cajaLogin">
        <h1 class="text-center">Formación complementaria</h1>
        <hr>
        <div class="row" style="padding: 15px;">
            <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <?
                echo $comp->nombre
                ?>
            </div>


        </div>


    </div>
    <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12 cajaLogin">
        <h1 class="text-center">Experencia laboral</h1>
        <hr>
        <div class="row" style="padding: 15px;">
            <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <?
                foreach ($exp as $value) {
                    ?>
                    <div class="row">
                        <div class="col-sm-12 col-md-12 col-lg-3 col-xl-3">
                            <img class="img-thumbnail" src="<?= base_url("media/background/") . $value->imagen ?>">
                        </div>
                        <div class="col-sm-12 col-md-12 col-lg-9 col-xl-9">
                            <p><?= $value->nombre ?></p>
                            <p><?
                                $datetime1=new DateTime($value->fecha_incio);

                                $datetime2=new DateTime($value->fecha_final);
                                # obtenemos la diferencia entre las dos fechas
                                $interval=$datetime2->diff($datetime1);
                                # obtenemos la diferencia en meses
                                $intervalMeses=$interval->format("%m");
                                # obtenemos la diferencia en años y la multiplicamos por 12 para tener los meses
                                $intervalAnos = $interval->format("%y");

                                if($intervalAnos!="0"){?>
                                <span>(<?=$intervalAnos." años y ".$intervalMeses." meses"?>)</span></p>
                            <?}else{?>
                                <span>(<?=$intervalMeses." meses"?>)</span></p>
                            <?}
                            ?>
                        </div>
                    </div>
                    <hr>


                    <?
                }
                ?>

            </div>


        </div>


    </div>
    <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12 cajaLogin">
        <h1 class="text-center">Infórmatica</h1>
        <hr>
        <div class="row" style="padding: 15px;">
            <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <?
                foreach ($computing as $value) {
                    ?>
                    <div class="col-12">
                        <p><?= $value->nombre ?></p>
                        <div class="progress">
                            <div class="progress-bar" role="progressbar" style="width: <?=$value->nivel?>%; height: 1px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                        <div class="progress">
                            <div class="progress-bar" role="progressbar" style="width: <?=$value->nivel?>%; height: 20px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"><?=$value->nivel?>%</div>
                        </div>
                        <hr>
                    </div>


                    <?
                }
                ?>

            </div>


        </div>


    </div>
    <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12 cajaLogin">
        <h1 class="text-center">Idiomas</h1>
        <hr>
        <div class="row" style="padding: 15px;">
            <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <?
                foreach ($lang as $value) {
                    ?>
                    <div class="col-12">
                        <p><?= $value->nombre ?></p>
                        <div class="progress">
                            <div class="progress-bar" role="progressbar" style="width: <?=$value->nivel?>%; height: 1px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                        <div class="progress">
                            <div class="progress-bar" role="progressbar" style="width: <?=$value->nivel?>%; height: 20px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"><?=$value->nivel?>%</div>
                        </div>
                        <hr>
                    </div>


                    <?
                }
                ?>

            </div>


        </div>


    </div>


</main>

<a href="#" class="scrollup">Scroll</a>
<script>
    $(document).ready(function () {

        $(window).scroll(function () {
            if ($(this).scrollTop() > 100) {
                $('.scrollup').fadeIn();
            } else {
                $('.scrollup').fadeOut();
            }
        });

        $('.scrollup').click(function () {
            $("html, body").animate({scrollTop: 0}, 600);
            return false;
        });

    });
</script>

