<nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand" href="<?=base_url()?>">
        <img src="<?=base_url("media/ico/wattpad.png")?>" width="30" height="30" class="d-inline-block align-top" alt="">
        CBP
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
               <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Action
                </a>
                <div class="dropdown-menu dropdown-info" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item" href="<?=base_url("contact")?>"> <i class="fa fa-envelope"></i> Contacto</a>
                    <a class="dropdown-item" href="#"> <i class="fa fa-file-pdf-o"></i> Descargar PDF</a>

                </div>
            </li>
        </ul>

    </div>
</nav>